#!/bin/bash
#
# zenbench.sh is a compilation of one liner bash compression benchmarks
# It's purpose is to measure padded zero content compression bandwidth for:
# - single thread compression algorithms
# - multi threads compression algorithms
# - scalability of multi threads compression algorithms
#
# It can be used as a real good indicator of cpu/ram efficiency
# 
# version 0.2
# zentoo - 2019/08/02

requirements ()
{
  STOP=0
  for x in lshw pv dd gzip bzip2 xz pxz pixz zip lzop lz4 pigz plzip pbzip2 lbzip2 ; do
    if ! command -v $x >/dev/null ; then
      echo "You need to have $x installed"
      STOP=1
    fi
  done
  [[ $STOP == 1 ]] && exit $STOP
}

cpuinfo ()
{
  echo
  echo "=== CPU ==="
  lshw -class cpu | grep -E "product:|slot:" | sed 's|.*product: ||;s|.*slot:|Socket:|'
  lshw -class cpu | grep -E "size:|capacity:" | tail -1 | sed "s|.*:|Speed:|"
  lshw -class memory -short | grep -E "cache" | sed 's|.*memory *||g'
  wait
}

meminfo ()
{
  echo
  echo "=== RAM ==="
  lshw -class memory -short | grep -E "System Memory|DDR" | sed 's|.*memory *||g'
}


benchs ()
{
  echo
  echo "=== Single thread benchmarks ==="
  echo
  SIZE=16G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N dd > /dev/null
  SIZE=1G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N gzip | /bin/gzip > /dev/null
  SIZE=1G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N bzip2 | /bin/bzip2 > /dev/null
  SIZE=512M ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N xz | xz > /dev/null
  SIZE=1G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N zip | zip -q > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N lzop | lzop > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N lz4 | lz4 > /dev/null

  echo
  echo "=== Multi threads benchmarks ==="
  echo
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N pigz | pigz > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N plzip | plzip > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N pbzip2 | pbzip2 > /dev/null
  SIZE=16G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N lbzip2 | lbzip2 > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N xz | xz -T0 > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N pxz | pxz > /dev/null
  SIZE=8G ; dd if=/dev/zero bs=$SIZE count=1 2>/dev/null | pv -aWi0.1 -N pixz | pixz > /dev/null

  echo
  echo "=== Threads scalability benchmarks ===" 
  echo
  SIZE=512 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "pigz $p/$(nproc)" | pigz -p$p > /dev/null ; done
  echo
  SIZE=128 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "plzip $p/$(nproc)" | plzip -n$p > /dev/null ; done
  echo
  SIZE=512 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "pbzip2 $p/$(nproc)" | pbzip2 -p$p > /dev/null ; done
  echo
  SIZE=1024 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "lbzip2 $p/$(nproc)" | lbzip2 -n$p > /dev/null ; done
  echo
  SIZE=256 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "xz $p/$(nproc)" | xz -T$p > /dev/null ; done
  echo
  SIZE=256 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "pxz $p/$(nproc)" | pxz -T$p > /dev/null ; done
  echo
  SIZE=256 ; for p in $(seq $(nproc)); do dd if=/dev/zero count=1 bs=$(( $SIZE * $p ))M 2>/dev/null | pv -aWi0.1 -N "pixz $p/$(nproc)" | pixz -p$p > /dev/null ; done
  echo
}

# MAIN
requirements
cpuinfo
meminfo
benchs
